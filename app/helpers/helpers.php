<?php

// custom Responses
require 'response.php';

// custom Validations
//require 'customValidations.php';

if (!function_exists('validate_base64')) {

    /**
     * Validate a base64 content.
     *
     * @param string $base64data
     * @param array $allowedMime example ['png', 'jpg', 'jpeg']
     * @return bool
     */
    function validate_base64($base64data, array $allowedMime)
    {
        // strip out data uri scheme information (see RFC 2397)
        if (strpos($base64data, ';base64') !== false) {
            list(, $base64data) = explode(';', $base64data);
            list(, $base64data) = explode(',', $base64data);
        }

        // strict mode filters for non-base64 alphabet characters
        if (base64_decode($base64data, true) === false) {
            return false;
        }
        // decoding and then reeconding should not change the data
        if (base64_encode(base64_decode($base64data)) !== $base64data) {
            return false;
        }
        $binaryData = base64_decode($base64data);
        // temporarily store the decoded data on the filesystem to be able to pass it to the fileAdder
        $tmpFile = tempnam(sys_get_temp_dir(), 'medialibrary');
        file_put_contents($tmpFile, $binaryData);
        // guard Against Invalid MimeType
        $allowedMime = \Arr::flatten($allowedMime);
        // no allowedMimeTypes, then any type would be ok
        if (empty($allowedMime)) {
            return true;
        }
        // Check the MimeTypes
        $validation = Illuminate\Support\Facades\Validator::make(
            ['file' => new Illuminate\Http\File($tmpFile)],
            ['file' => 'mimes:' . implode(',', $allowedMime)]
        );
        return !$validation->fails();
    }
}

if (!function_exists('base64Store')) {
    function base64Store($file)
    {
        if (preg_match('/^data:image\/(\w+);base64,/', $file)) {
            $folderPath = "storage/";
            $image_parts = explode(";base64,", $file);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $file = $folderPath . uniqid('', true) . '.' . $image_type;
            file_put_contents($file, $image_base64);
            return $file;
        }
        return false;
    }
}
if (!function_exists('get_base64_ext')) {
    function get_base64_ext($base64)
    {
        $image_info = getimagesize($base64);
        return (isset($image_info["mime"]) ? explode('/', $image_info["mime"])[1] : "");
    }
}


if (!function_exists('base64_encode_image')) {
    function base64_encode_image($imagePath)
    {
        try {
            $type = pathinfo($imagePath, PATHINFO_EXTENSION);
            $data = file_get_contents($imagePath);
            return 'data:image/' . $type . ';base64,' . base64_encode($data);
        } catch (Exception $e) {
            return null;
        }
    }
}

if (!function_exists('isBase64Image')) {
    function isBase64Image($filename, $filetype)
    {
        if ($filename) {
            $imgbinary = fread(fopen($filename, "r"), filesize($filename));
            return 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
        }
        return false;
    }
}

if (!function_exists('validate_url')) {
    function validate_url($url)
    {
        $path = parse_url($url, PHP_URL_PATH);
        $encoded_path = array_map('urlencode', explode('/', $path));
        $url = str_replace($path, implode('/', $encoded_path), $url);
        return filter_var($url, FILTER_VALIDATE_URL) ? true : false;
    }
}

