<?php

if (!function_exists('paginatedJsonResponse')) {
    function paginatedJsonResponse($message = "", $data = null, $code = 200)
    {
        $responseData = [
            "items" => $data['items'],
            "count" => (int)$data['items']->count(),
            "total" => (int)$data['items']->total(),
            "last_page" => (int)$data['items']->lastPage(),
            "per_page" => (int)$data['items']->perPage(),
            "current_page" => (int)$data['items']->currentPage(),
            "get_options" => $data['items']->getOptions(),
            "next_page_url" => $data['items']->nextPageUrl(),
        ];
        unset($data['items']);
        foreach ($data as $key => $val) {
            $responseData[$key] = $val;
        }
        return response()->json([
            "code" => $code,
            "message" => $message,
            "data" => $responseData
        ], 200);
    }
}
if (!function_exists('jsonResponse')) {
    function jsonResponse($message = "", $data = null, $code = 200)
    {
        return response()->json([
            "code" => $code,
            "message" => $message,
            "data" => $data,
        ], $code);
    }
}
if (!function_exists('errorResponse')) {
    function errorResponse($userMessage, $internalMessage, $moreInfo = [], $code = 400)
    {
        return response()->json([
            "code" => $code,
            "message" => $userMessage,
            "errors" => [
                "errorMessage" => $internalMessage,
                "errorDetails" => $moreInfo,
            ]
        ], $code);
    }
}


