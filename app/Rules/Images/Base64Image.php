<?php

namespace App\Rules\Images;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\File;

class Base64Image implements Rule
{
    private $allowedMime;
    private $maxSize;
    private $baseOrLink;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(array $allowedMime = ['jpeg', 'png', 'jpg'], $maxSize = '204800', $baseOrLink = false)
    {
        $this->allowedMime = $allowedMime;
        $this->maxSize = $maxSize;
        $this->baseOrLink = $baseOrLink;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $base64data)
    {
        // strip out data uri scheme information (see RFC 2397)
        if (strpos($base64data, ';base64') !== false) {
            list(, $base64data) = explode(';', $base64data);
            list(, $base64data) = explode(',', $base64data);
        }

        // strict mode filters for non-base64 alphabet characters
        if (base64_decode($base64data, true) === false) {
            return false;
        }

        // decoding and then reeconding should not change the data
        if (base64_encode(base64_decode($base64data)) !== $base64data) {
            return false;
        }

        if (preg_match('/^data:image\/(\w+);base64,/', $base64data)) {
            return false;
        }

        $binaryData = base64_decode($base64data);

        // temporarily store the decoded data on the filesystem to be able to pass it to the fileAdder
        $tmpFile = tempnam(sys_get_temp_dir(), 'medialibrary');
        file_put_contents($tmpFile, $binaryData);

        // guard Against Invalid MimeType
        $allowedMime = Arr::flatten($this->allowedMime);

        // no allowedMimeTypes, then any type would be ok
        if (empty($allowedMime)) {
            return true;
        }
        $file = new File($tmpFile);
        $mimeValidateData = 'mimes:' . implode(',', $allowedMime);
        //dd($mimeValidateData,$file->getMimeType());
        // Check the MimeTypes
        $validation = Validator::make(
            ['file' => new File($tmpFile)],
            ['file' => $mimeValidateData]
        );
        return !$validation->fails();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The File Is Not Acceptable , Only Allowed Extensions is : ' . implode(',', $this->allowedMime);
    }
}
