<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use function Symfony\Component\String\u;

class PostImage extends Model
{
    protected $fillable = [
        'name',
        'path',
        'post_id',
    ];

    public function post()
    {
        $this->belongsTo(Post::class);
    }

    public function getPathAttribute($imagePath)
    {
        return base64_encode_image($imagePath);
    }

}
