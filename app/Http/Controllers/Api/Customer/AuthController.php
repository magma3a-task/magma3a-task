<?php

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Login With Social Media
     *
     * @param  $provider
     * @return User
     */
    public function socialLogin($provider)
    {
        if ($provider != 'google')
            abort(404);
        try {
            $user = $this->repository->loginWithSocial($provider);
            $message = 'Done .. Welcome';
            $accessToken = $user->createToken('authToken')->accessToken;
            $data = [
                'access_token' => $accessToken,
                'user_data' => new UserResource($user)
            ];
            return jsonResponse($message, $data);
        } catch (\Exception $exception) {
            $message = "Some Thing Went Wrong";
            return errorResponse($message, $message, null, 422);
        }
        return response()->json(compact('token', 'avatar'));
    }

    /**
     * Login With User Credentials
     *
     * @param Request $request
     * @return User
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|exists:users,email',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails())
            return errorResponse($validator->errors()->first(), "Invalid Login Data", $validator->errors(), 422);
        $user = $this->repository->login($validator->valid());
        if (!$user)
            return errorResponse("Invalid Login Data", 422);
        $data = [
            'access_token' => $user->createToken('authToken')->accessToken,
            'user_data' => new UserResource($user)
        ];
        return jsonResponse("Done .. Welcome", $data);
    }

    /**
     * register With User Credentials
     *
     * @param Request $request
     * @return User
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|min:3',
            'last_name' => 'required|string|min:3',
            'email' => 'required|string|email|unique:users,email',
            'password' => 'required|string|min:6|confirmed',
        ]);
        if ($validator->fails())
            return errorResponse($validator->errors()->first(), "'Invalid Register Data'", $validator->errors(), 422);
        $user = $this->repository->register($validator->valid());
        if (!$user)
            return errorResponse("Invalid Registration Data", 422);
        $data = [
            'access_token' => $user->createToken('authToken')->accessToken,
            'user_data' => new UserResource($user)
        ];
        return jsonResponse("Done .. Welcome", $data, 201);
    }

    /**
     * Logout User
     *
     * @param  $provider
     * @return User Data
     */
    public function logout()
    {
        $this->repository->logout(auth()->User());
        return jsonResponse('Logout successfully', null);
    }
}
