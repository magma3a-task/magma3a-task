<?php

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Repositories\PostRepository;
use App\Repositories\UserRepository;
use App\Rules\Images\Base64Image;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;

class PostsController extends Controller
{
    /**
     * @var PostRepository
     */
    private $repository;

    /**
     * @param PostRepository $repository
     */
    public function __construct(PostRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Get All Posts Pagination With Some Statistics
     *
     * @return void
     */
    public function index()
    {
        $posts['items'] = PostResource::collection($this->repository->getAllPosts());
        return paginatedJsonResponse("Done", $posts);
    }

    /**
     * Get User Posts Pagination With Some Statistics
     *
     * @return void
     */
    public function userPosts()
    {
        $posts['items'] = PostResource::collection($this->repository->getUserPosts(auth()->User()));
        return paginatedJsonResponse("Done", $posts);
    }

    /**
     * Show Post Details
     *
     * @param $id
     * @return void
     */
    public function show($id)
    {
        $post = $this->repository->show($id);
        return jsonResponse("success", new PostResource($post), 200);
    }

    /**
     * Delete Post
     *
     * @param $id
     * @return void
     */
    public function destroy($id)
    {
        $isDeleted = $this->repository->destroy($id);
        if (!$isDeleted)
            return errorResponse("Failed To Delete Post", 422);
        return jsonResponse("success", null, 200);
    }

    /**
     * Create New Post
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'images' => 'required|array|filled',
            'images.*.name' => 'required|string',
            'images.*.path' => ['required', new Base64Image(['jpeg', 'png', 'jpg'], '204800', true)],
        ]);
        if ($validator->fails())
            return errorResponse($validator->errors()->first(), "Invalid Data", $validator->errors(), 400);
        $data = $validator->valid();
        $post = $this->repository->storeItem($data);
        return jsonResponse("Data Saved Successfully ", new PostResource($post), 201);
    }

    /**
     * Update Post
     *
     * @param $id
     * @param Request $request
     * @return void
     */
    public function update(Request $request, $id)
    {
        $item = $this->repository->getUserPost(auth()->User(), $id);
        if (!$item)
            return errorResponse('Item Not Found', "Invalid Data", 422);
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'images' => 'required|array|filled',
            'images.*.name' => 'required|string',
            'images.*.path' => ['required', new Base64Image(['jpeg', 'png', 'jpg'], '204800', true)],
        ]);
        if ($validator->fails())
            return errorResponse($validator->errors()->first(), "Invalid Data", $validator->errors(), 400);
        $data = $validator->valid();
        $post = $this->repository->updateItem($item, $data);
        return jsonResponse("Data Saved Successfully ", new PostResource($post), 202);
    }
}
