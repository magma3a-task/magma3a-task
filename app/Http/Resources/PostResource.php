<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'content' => $this->content,
            'user_id' => $this->user_id,
            'user' => new UserResource($this->user),
            'images' => PostImagesResource::collection($this->images),
            'image' => $this->images->first()->path ?? url('images/placeholder.png'),
            // 'images_list' => $this->images->pluck('url')->toArray(),
        ];
    }
}
