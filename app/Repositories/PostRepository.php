<?php

namespace App\Repositories;


use App\Models\Post;
use App\Models\PostImage;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;

class PostRepository
{
    protected $module;

    public function __construct()
    {
        $class = Post::class;
        $this->module = new $class;
    }

    /**
     *  * Get All Posts
     *
     * @return void
     */
    public function getAllPosts()
    {
        return $this->module::latest()->paginate(10);
    }

    /**
     *  * Get User All Posts
     * @param User $user
     * @return void
     */
    public function getUserPosts(User $user)
    {
        return $user->posts()->latest()->paginate(10);
    }

    /**
     *  * Get User Post
     * @param User $user
     * @param $id
     * @return void
     */
    public function getUserPost(User $user, $id)
    {
        return $user->posts()->where('id', $id)->first();
    }

    /**
     *  * Get Post
     * @param $id
     * @return void
     */
    public function show(int $id)
    {
        return $this->module::findOrFail($id);
    }

    /**
     *  Delete Post
     * @param $id
     * @return void
     */
    public function destroy(int $id)
    {
        $user = auth()->User();
        $post = $user->posts()->find($id);
        if (!$post)
            return false;
        return $post->delete();
    }

    /**
     *  * Create User Post
     * @param $data // POst Details
     * @return Post
     */
    public function storeItem($data)
    {
        $data['user_id'] = auth()->User()->id;
        $post = $this->module::create($data);
        $this->storeItemImages($data['images'], $post);
        return $post;
    }

    /**
     *  * Update User Post
     * @param Post $post // POst That Would Be Updated
     * @param $data // POst Details
     * @return Post
     */
    public function updateItem(Post $post, $data)
    {
        $post->update($data);
        $this->storeItemImages($data['images'], $post, true);
        return $post;
    }

    /**
     *  * Update Post Images
     * @param $images //Images With Base 64 encode
     * @param Post $post // POst That Would Be Updated
     * @param $deleteOldImages // Would u like to delete old images or not ?
     * @return void
     */
    public function storeItemImages(array $images, Post $post, bool $deleteOldImages = false): void
    {
        if ($deleteOldImages) {
            foreach ($post->images as $image) {
                $image = $image->getRawOriginal('path');
                $postImage = "/public" . str_replace('storage', '', $image);
                $deleted = Storage::delete($postImage);
            }
            $post->images()->delete();
        }
        if (is_array($images) && count($images) > 0) {
            foreach ($images as $image) {
                $storeImage = base64Store($image['path']);
                $post->images()->create([
                    'name' => $image['name'],
                    'path' => $storeImage,
                ]);
            }
        }
    }

}
