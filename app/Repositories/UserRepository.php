<?php

namespace App\Repositories;


use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;

class UserRepository
{
    /**
     * @return Collection
     */
    public function getUsers(): Collection
    {
        return User::all();
    }

    /**
     * @param int $userId
     * @return User
     */
    public function getUserById(int $userId): User
    {
        return User::find($userId);
    }

    /**
     * @param string $email
     * @return User
     */
    public function getUserByEmail(string $email)
    {
        return User::where('email', $email)->first();
    }


    /**
     * @param string $provider
     * @return User
     */
    public function loginWithSocial(string $provider): User
    {
        $auth = Socialite::driver($provider)->stateless()->user();
        $user = $this->getUserByEmail($auth->getEmail());
        if (!$user) {
            $user = User::create([
                "name" => $auth->getName(),
                "email" => $auth->getEmail(),
                "email_verified_at" => now(),
                "password" => Hash::make(Str::random(15)),
            ]);
        }
        return $user;
    }

    /**
     * @param array $data
     * @return User
     */
    public function login(array $data)
    {
        $user = $this->getUserByEmail($data['email']);
        if (!$user || !Hash::check($data['password'], $user->password)) {
            return false;
        }
        return $user;
    }

    /**
     * @param array $data
     * @return User
     */
    public function register(array $data)
    {
        $user = $this->getUserByEmail($data['email']);
        if ($user)
            return false;
        return User::create([
            "name" => $data['first_name'] . ' ' . $data['last_name'],
            "email" => $data['email'],
            "email_verified_at" => now(),
            "password" => Hash::make($data['password']),
        ]);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function logout(User $user)
    {
        $user->token()->revoke();
        return true;
    }
}
