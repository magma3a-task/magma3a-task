<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{

    protected $apiNamespace;
    protected $webNamespace;
    protected $customerApiNamespace;
    /**
     * The controller namespace for the application.
     *
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    // protected $namespace = 'App\\Http\\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->webNamespace = 'App\Http\Controllers\Web';
        $this->apiNamespace = 'App\Http\Controllers\Api\Customer';
        $this->customerApiNamespace = 'App\Http\Controllers\Api\Customer';

        $this->configureRateLimiting();
        $this->mapApiRoutes();
        $this->mapCustomerApiRoutes();
        $this->mapCustomerAuthApiRoutes();
        $this->mapWebRoutes();
    }


    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->webNamespace)
            ->prefix('')
            ->group(base_path('routes/web.php'));
    }

    protected function mapApiRoutes()
    {
        Route::middleware('api')
            ->namespace($this->apiNamespace)
            ->prefix('/api')
            ->group(base_path('routes/api.php'));
    }

    protected function mapCustomerApiRoutes()
    {
        Route::middleware('api')
            ->namespace($this->customerApiNamespace)
            ->prefix('/api/customer')
            ->group(base_path('routes/customerApi.php'));
    }

    protected function mapCustomerAuthApiRoutes()
    {
        Route::middleware(['api','customer'])
            ->namespace($this->customerApiNamespace)
            ->prefix('/api/customer')
            ->group(base_path('routes/customerAuthApi.php'));
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });
    }
}
