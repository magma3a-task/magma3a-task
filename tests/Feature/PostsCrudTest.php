<?php

namespace Tests\Feature;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class PostsCrudTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic test example.
     *
     * @return void
     */

    public $user;
    public $post;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->post = Post::factory()->create(['user_id' => $this->user->id]);
    }

    /** @test */
    public function guest_can_browser_posts()
    {
        $postsCount = Post::count();
        $response = $this->get('/api/customer/posts');
        $result = $response->json();
        $this->assertEquals($result['code'], 200);
        $this->assertEquals($this->count($result['data']['total']), $postsCount);
        $this->assertTrue($this->count($result['data']['items']) > 0);
    }

    /** @test */
    public function guest_can_view_post()
    {
        $response = $this->get('/api/customer/posts/' . $this->post->id);
        $this->assertEquals($response['code'], 200);
        $this->assertEquals($response['data']['title'], $this->post->title);
        $this->assertEquals($response['data']['content'], $this->post->content);
    }

    /** @test */
    public function user_can_view_post()
    {
        $response = $this->actingAs($this->user)->get('/api/customer/posts/' . $this->post->id);
        $this->assertEquals($response['code'], 200);
        $this->assertEquals($response['data']['title'], $this->post->title);
        $this->assertEquals($response['data']['content'], $this->post->content);
    }

    /** @test */
    public function user_can_browser_his_posts()
    {
        $postsCount = Post::where('user_id', $this->user->id)->count();
        $response = $this->actingAs($this->user)->get('/api/customer/my-posts');
        $result = $response->json();
        $this->assertEquals($result['code'], 200);
        $this->assertEquals($this->count($result['data']['total']), $postsCount);
        $this->assertTrue($this->count($result['data']['items']) > 0);
    }

    /** @test */
    public function user_can_create_post()
    {
        $data = [
            'title' => "post Title",
            'content' => "post Content",
            'images' => [
                [
                    "name" => "image",
                    "path" => base64_encode_image(url('images/placeholder.png'))
                ]
            ],
            'user_id' => $this->user->id,
        ];
        $response = $this->actingAs($this->user)->post('/api/customer/posts', $data);
        $response->assertStatus(201);
        $this->assertEquals($response['code'], 201);
        $this->assertEquals($data['title'], $response['data']['title']);
        $this->assertEquals($data['content'], $response['data']['content']);

    }

    /** @test */
    public function user_can_create_post_without_title()
    {
        $data = [
            'content' => "post Content",
            'images' => [
                [
                    "name" => "image",
                    "path" => base64_encode_image(url('images/placeholder.png'))
                ]
            ],
            'user_id' => $this->user->id,
        ];
        $response = $this->actingAs($this->user)->post('/api/customer/posts', $data);
        $response->assertStatus(400);
        $this->assertEquals($response['code'], 400);
        $this->assertEquals("The title field is required.", $response['message']);
    }

    /** @test */
    public function user_can_update_post()
    {
        $data = [
            'title' => "Updated Title",
            'content' => "Updated Content",
            'images' => [
                [
                    "name" => "image",
                    "path" => base64_encode_image(url('images/placeholder.png'))
                ]
            ],
            'user_id' => $this->user->id,
        ];
        $response = $this->actingAs($this->user)->put('/api/customer/posts/' . $this->post->id, $data);
        $response->assertStatus(202);
        $this->assertEquals($response['code'], 202);
        $this->assertEquals($data['title'], $response['data']['title']);
        $this->assertEquals($data['content'], $response['data']['content']);
    }

    /** @test */
    public function user_can_delete_post()
    {
        $response = $this->actingAs($this->user)->delete('/api/customer/posts/' . $this->post->id);
        $response->assertStatus(200);
        $deletedPost = Post::find($this->post->id);
        $this->assertEquals($response['code'], 200);
        $this->assertNull($deletedPost);
    }

    /** @test */
    public function guest_cannot_create_post()
    {
        $data = [
            'title' => "post Title",
            'content' => "post Content",
            'images' => [
                [
                    "name" => "image",
                    "path" => base64_encode_image(url('images/placeholder.png'))
                ]
            ],
            'user_id' => $this->user->id,
        ];
        $response = $this->post('/api/customer/posts', $data);
        $response->assertStatus(401);
        $this->assertEquals("Unauthorized access", $response['message']);
        $this->assertEquals($response['code'], 401);

    }

    /** @test */
    public function guest_cannot_update_post()
    {
        $data = [
            'title' => "Updated Title",
            'content' => "Updated Content",
            'images' => [
                [
                    "name" => "image",
                    "path" => base64_encode_image(url('images/placeholder.png'))
                ]
            ],
            'user_id' => $this->user->id,
        ];
        $response = $this->put('/api/customer/posts/' . $this->post->id, $data);
        $response->assertStatus(401);
        $this->assertEquals("Unauthorized access", $response['message']);
        $this->assertEquals($response['code'], 401);
    }

    /** @test */
    public function guest_cannot_delete_post()
    {
        $response = $this->delete('/api/customer/posts/' . $this->post->id);
        $response->assertStatus(401);
        $this->assertEquals("Unauthorized access", $response['message']);
        $this->assertEquals($response['code'], 401);
    }
}
