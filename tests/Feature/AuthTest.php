<?php

namespace Tests\Feature;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use DatabaseMigrations;


    public $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
    }

    /** @test */
    public function can_register_customer()
    {
        $this->withoutExceptionHandling();
        Artisan::call("passport:install");

        //User's data
        $data = [
            'first_name' => 'test',
            'last_name' => 'test',
            'email' => 'test@gmail.com',
            'password' => 'secret1234',
            'password_confirmation' => 'secret1234',
        ];
        //Send post request
        $response = $this->json('POST', '/api/customer/register', $data);
        //Assert it was successful
        $this->assertEquals(201, $response['code']);
        $this->assertEquals($data['email'], $response['data']['user_data']['email']);
    }

    /** @test */
    public function can_register_name_required()
    {

        $data = [
            // 'first_name' => 'test',
            'last_name' => 'test',
            'email' => 'test@gmail.com',
            'password' => 'secret1234',
            'password_confirmation' => 'secret1234',
        ];

        //Send post request
        $response = $this->json('POST','/api/customer/register',$data);
        $this->assertEquals($response['code'], 422);
        $user = User::where('email',$data['email'])->get();
        $this->assertCount(0, $user);

    }

}
