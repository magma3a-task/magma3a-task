import Vue from "vue";
import router from './router';
import store from "./store/store";
import VeeValidate from 'vee-validate';
import VueAxios from 'vue-axios'
import Vuesax from 'vuesax'
import '@fortawesome/fontawesome-free/js/all.js';
import VueSocialauth from 'vue-social-auth'
import VueSweetalert2 from 'vue-sweetalert2';
import VueLazyload from 'vue-lazyload'

import App from './pages/App';
Vue.use(VueLazyload)

require('./bootstrap');
import 'sweetalert2/dist/sweetalert2.min.css';
import 'vuesax/dist/vuesax.css' //Vuesax styles
Vue.use(VueSweetalert2);
Vue.use(Vuesax)
Vue.use(VeeValidate);
Vue.use(VueAxios, axios);


axios.interceptors.response.use(response => {
    return response
}, error => {
    if (error.response.status === 401) {
        store.commit('moduleAuth/logOut', error.response)
        router.push({name: 'login'})
    }
    return error
})
axios.defaults.headers.common['Authorization'] = localStorage.getItem('access_token')


Vue.use(VueSocialauth, {
    providers: {
        google: {
            clientId: process.env.MIX_GOOGLE_CLIENT_ID,
            client_secret: process.env.MIX_GOOGLE_CLIENT_SECRET_KEY,
            redirectUri: `${process.env.MIX_APP_BASE_URL}/auth/google/callback`,
            // redirectUri: `${process.env.MIX_APP_BASE_URL}/api/customer/login/google/callback`,
            // redirectUri: '/auth/github/callback',
        }
    }
});
const app = new Vue({
    el: '#app',
    components: {App},
    router,
    store
});
