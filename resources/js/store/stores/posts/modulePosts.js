import actions from './modulePostsActions'
import getters from './modulePostsGetters'
import mutations from './modulePostsMutations'
import state from './modulePostsState'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
}
