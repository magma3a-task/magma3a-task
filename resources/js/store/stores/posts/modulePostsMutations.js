export default {
    setItems(state, data) {
        state.items = data
    },
    setItem(state, data) {
        state.item = data
    },
    removeItem(state, itemId) {
        const ItemIndex = state.items.findIndex(p => p.id === itemId)
        state.items.splice(ItemIndex, 1)
    },
}
