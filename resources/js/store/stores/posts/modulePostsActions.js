import axios from 'axios'

const moduleName = 'Posts'
const modulePath = '/api/customer/posts'
export default {
    fetchItems({commit}, showData) {
        return new Promise((resolve, reject) => {
            axios.get(modulePath, {params: showData})
                .then(response => {
                    if (response.status && response.status === 200) {
                        commit('setItems', response.data.data.items)
                        resolve(response)
                    } else {
                        reject(response.response)
                    }

                })
                .catch(error => reject(error.response))
        })
    },
    fetchItem({commit}, payload) {
        return new Promise((resolve, reject) => {
            axios.get(`${modulePath}/${payload.post_id}`)
                .then(response => {
                    if (response.status && response.status === 200) {
                        console.log(response.data)
                        commit('setItem', response.data.data)
                        resolve(response)
                    } else {
                        reject(response.response)
                    }

                })
                .catch(error => reject(error.response))
        })
    },
    fetchMyItems({commit}, showData) {
        return new Promise((resolve, reject) => {
            axios.get('/api/customer/my-posts', {params: showData})
                .then(response => {
                    if (response.status && response.status === 200) {
                        commit('setItems', response.data.data.items)
                        resolve(response)
                    } else {
                        reject(response.response)
                    }

                })
                .catch(error => reject(error.response))
        })
    },
    updateItem({commit}, payload) {
        return new Promise((resolve, reject) => {
            axios.put(`${modulePath}/${payload.id}`, payload)
                .then(response => response.status && response.status === 202 ? resolve(response) : reject(response.response))
                .catch(error => reject(error.response))
        })
    },
    createItem({commit}, payload) {
        return new Promise((resolve, reject) => {
            axios.post(`${modulePath}`, payload)
                .then(response => response.status && response.status === 201 ? resolve(response) : reject(response.response))
                .catch(error => reject(error.response))
        })
    },
    deleteItem({commit}, itemID) {
        return new Promise((resolve, reject) => {
            axios.delete(`${modulePath}/${itemID}`)
                .then(response => response.status && response.status === 200 ? resolve(response) : reject(response.response))
                .catch(error => reject(error.response))
        })
    },

}
