export default {
    items(state) {
        return state.items
    },
    item(state) {
        return state.item
    },
}
