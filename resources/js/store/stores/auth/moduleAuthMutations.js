import axios from "axios";

export default {
    loginSuccess(state, payload) {
        const {user_data, access_token} = payload
        state.user_data = user_data
        localStorage.setItem('user_data', JSON.stringify(user_data))
        state.access_token = `Bearer ${access_token}`
        localStorage.setItem('access_token', `Bearer ${access_token}`)
        axios.defaults.headers.common['Authorization'] = `Bearer ${access_token}`
    },
    updateUserData(state, payload) {
        state.user_data = payload
        localStorage.setItem('user_data', JSON.stringify(payload))
    },
    updateUserToken(state, payload) {
        state.access_token = `Bearer ${payload}`
        localStorage.setItem('access_token', `Bearer ${payload}`)
        axios.defaults.headers.common['Authorization'] = `Bearer ${payload}`
    },
    registerSuccess(state, payload) {
        const {user_data, access_token} = payload
        state.user_data = user_data
        localStorage.setItem('user_data', JSON.stringify(user_data))

        state.access_token = `Bearer ${access_token}`
        localStorage.setItem('access_token', `Bearer ${access_token}`)
        axios.defaults.headers.common['Authorization'] = `Bearer ${access_token}`
    },
    forgetPassword(state, payload) {

    },
    resetPassword(state, payload) {

    },
    logOut(state) {
        localStorage.removeItem('access_token')
        localStorage.removeItem('user_data')
        state.user_data = null
        state.access_token = null
        axios.defaults.headers.common.Authorization = null
    },
    setAxiosDefaultBearer(state, access_token) {
        axios.defaults.headers.common.Authorization = `Bearer ${access_token}`
    },
}
