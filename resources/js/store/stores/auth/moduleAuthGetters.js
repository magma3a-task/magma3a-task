export default {
    access_token(state) {
    return state.access_token
  },
  is_logged_in(state) {
    return state.access_token && state.user_data !== null
  },
  user_data(state) {
    return state.user_data
  },
}
