export default {
  access_token: localStorage.getItem('access_token') || null,
  user_data: JSON.parse(localStorage.getItem('user_data')) || null,
}
