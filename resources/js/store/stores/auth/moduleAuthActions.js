import axios from "axios";

export default {
    login(context, payload) {
        return new Promise((resolve, reject) => {
            axios.post(`${process.env.MIX_APP_BASE_URL}/api/customer/login`, payload)
                .then(response => {
                    if (response.data.code === 200) {
                        context.commit('loginSuccess', response.data.data)
                        resolve(response)
                    } else {
                        reject(response)
                    }
                })
                .catch(error => reject(error))
        })
    },
    socialLoginCallback(context, payload) {
        return new Promise((resolve, reject) => {
            axios.post(`${process.env.MIX_APP_BASE_URL}/api/customer/login/google/callback`, payload)
                .then(response => {
                    if (response.data.code === 200) {
                        context.commit('loginSuccess', response.data.data)
                        resolve(response)
                    } else {
                        reject(response)
                    }
                })
                .catch(error => reject(error))
        })
    },
    register(context, payload) {
        return new Promise((resolve, reject) => {
            axios.post(`${process.env.MIX_APP_BASE_URL}/api/customer/register`, payload)
                .then(response => {
                    if (response.status && response.status === 201) {
                        context.commit('registerSuccess', response.data.data)
                        resolve(response)
                    } else {
                        reject(response)
                    }
                })
                .catch(error => reject(error))
        })
    },
    logOut(context) {
        return new Promise((resolve, reject) => {
            axios.post(`${process.env.MIX_APP_BASE_URL}/api/customer/logout`)
                .then(response => {
                    context.commit('logOut', response.data.data)
                    resolve(response)
                })
                .catch(error => reject(error))
        })
    },
}
