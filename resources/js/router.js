import Vue from "vue";
import VueRouter from 'vue-router';
import HomeComponent from "./pages/HomeComponent";
import LoginComponent from "./pages/LoginComponent";
import RegisterComponent from "./pages/RegisterComponent";
import store from "./store/store";
import ViewPostComponent from "./pages/ViewPostComponent";
import MyPostsComponent from "./pages/MyPostsComponent";

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    // base: process.env.MIX_APP_BASE_URL,
    scrollBehavior() {
        return {x: 0, y: 0}
    },

    routes: [
        {
            path: '/',
            component: HomeComponent,
            name: "home",
            meta: {requiresGuest: false, requiresAuth: false,}
        },
        {
            path: '/login',
            component: LoginComponent,
            name: "login",
            meta: {requiresGuest: true, requiresAuth: false,}
        },
        {
            path: '/register',
            component: RegisterComponent,
            name: "register",
            meta: {requiresGuest: true, requiresAuth: false,}
        },
        {
            path: '/my-posts',
            component: MyPostsComponent,
            name: "my-posts",
            meta: {requiresGuest: false, requiresAuth: true,}
        },
        {
            path: '/posts/:item_id',
            component: ViewPostComponent,
            name: "view-post",
            meta: {requiresGuest: false, requiresAuth: false,}
        },
        {
            path: '/auth/:provider/callback',
            component: {template: '<div class="auth-component"></div>'}
        }
    ],
})
router.beforeEach((to, from, next) => {
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
    const requiresGuest = to.matched.some(record => record.meta.requiresGuest)
    const isLoggedIn = store.getters['moduleAuth/is_logged_in']
    if (!isLoggedIn && requiresAuth) {
        next({name: 'login'})
    } else if (isLoggedIn && requiresGuest) {
        next({name: 'home'})
    } else if (isLoggedIn && requiresAuth) {
        next()
    } else if (!isLoggedIn && requiresGuest) {
        next()
    } else {
        next()
    }
})

export default router
