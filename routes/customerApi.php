<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "customer no auth" middleware group. Enjoy building your API!
|
*/

Route::post('/login/{provider}/callback','AuthController@socialLogin');
Route::post('/login','AuthController@login');
Route::post('/register','AuthController@register');

Route::get('/posts','PostsController@index');
Route::get('/posts/{id}','PostsController@show');

