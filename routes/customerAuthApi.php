<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "customer auth" middleware group. Enjoy building your API!
|
*/
Route::post('/logout', 'AuthController@logout');
Route::get('/my-posts', 'PostsController@userPosts');
Route::resource('/posts', 'PostsController')->except(['index', 'show']);
