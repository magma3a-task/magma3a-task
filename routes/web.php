<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "web" middleware group. Enjoy building your API!
|
*/
// Route::get('auth/{provider}/callback', 'Customer\AppController@welcome')->where('provider', '.*');
Route::get('/{any}','Customer\AppController@index')->where('any','.*');

