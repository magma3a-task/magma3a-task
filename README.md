<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>


# Magma3a Tast

A simple project, logging in via Google, and also the client can write his own article and includes multiple images




## Installation

To Start project  You Should Follow This Steps


1- You Should Clone The Project Using This Command
```bash
  git clone https://vector0@bitbucket.org/magma3a-task/magma3a-task.git
```

2- copy .Envexample  File and rename it to .env

3- Make Sure Db Connection is working properly And Don't forget to edit the below keys
```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=magma3a_task
DB_USERNAME=root
DB_PASSWORD=
```

3- Make Sure To Put These Keys To .env File and please Note That We Will Serve Project on 127.0.0.1
if you want to serve in another host pleas edit on the below keys

```bash
APP_URL=http://127.0.0.1:8000


GOOGLE_ID=747164002298-2qo5043ei2b387mi2u84r37vh8sk090l.apps.googleusercontent.com
GOOGLE_SECRET=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

MIX_APP_TITLE_NAME=Vector
MIX_APP_NAME=Vector
MIX_APP_BASE_URL=http://127.0.0.1:8000
MIX_GOOGLE_CLIENT_SECRET_KEY=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
MIX_GOOGLE_CLIENT_ID=747164002298-2qo5043ei2b387mi2u84r37vh8sk090l.apps.googleusercontent.com
```

4- inside your project file please run These command

-  to install composer libraries
```bash
  composer install
```

- to put and seed Database Tables and data
```bash
 php artisan migrate:fresh --seed
```
- to generate And put Key in Env
```bash
php artisan key:generate
```
- to generate And passport Auth Keys in Database
```bash
php artisan passport:install
```


-  to install node modules
```bash
npm install
```

-  to Compile scripts
```bash
npm run dev
```




## Deployment


-  to start project
```bash
php artisan serve
```

to start project Open These Link In Your Browser and please make sure that u serve in the same link and the same port that u put inside .env file

```bash
  http://127.0.0.1:8000

```
## Support

For support, email mo.khaled.yousef@gmail.com .

