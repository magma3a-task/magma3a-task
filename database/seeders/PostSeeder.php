<?php

namespace Database\Seeders;

use App\Models\Post;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(0, 10) as $index) {
            $paragraphs = $faker->paragraphs(rand(2, 6));
            $title = $faker->realText(50);
            $post = "<h1>{$title}</h1>";
            foreach ($paragraphs as $para) {
                $post .= "<p>{$para}</p>";
            }
            $data = [
                'title' => $title,
                'content' => $post,
                'user_id' => 1,
            ];
            Post::create($data);
        }
    }
}
